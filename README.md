# nd2reader Test Data

### About

This is just some very large nd2 image files used for the functional testing of [nd2reader](https://github.com/jimrybarski/nd2reader).
If you just want to use nd2reader, there's no reason to download anything here.

### Usage

nd2reader will soon have a make command to run functional tests. It will require this repo to work. See the README at [nd2reader](https://github.com/jimrybarski/nd2reader) for details.